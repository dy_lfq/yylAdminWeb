import Cookies from 'js-cookie'

const AdminTokenKey = 'yylAdminAdminToken'
export function setAdminToken(AdminToken) {
  return Cookies.set(AdminTokenKey, AdminToken)
}
export function getAdminToken() {
  return Cookies.get(AdminTokenKey)
}
export function delAdminToken() {
  return Cookies.remove(AdminTokenKey)
}

const UsernameKey = 'yylAdminUsername'
export function setUsername(username) {
  return Cookies.set(UsernameKey, username)
}
export function getUsername() {
  return Cookies.get(UsernameKey)
}
export function delUsername() {
  return Cookies.remove(UsernameKey)
}

const NicknameKey = 'yylAdminNickname'
export function setNickname(nickname) {
  return Cookies.set(NicknameKey, nickname)
}
export function getNickname() {
  return Cookies.get(NicknameKey)
}
export function delNickname() {
  return Cookies.remove(NicknameKey)
}

const AvatarKey = 'yylAdminAvatar'
export function setAvatar(avatar) {
  return Cookies.set(AvatarKey, avatar)
}
export function getAvatar() {
  return Cookies.get(AvatarKey)
}
export function delAvatar() {
  return Cookies.remove(AvatarKey)
}
